
import json
from flask_api import FlaskAPI
from flask import request

import jellyfish

from spacy.en import English

nlp = English()

app = FlaskAPI(__name__)

with open('choices.json', 'r') as f:
    DISEASE_DATA = {d['title']:d for d in json.load(f)}
    diseases = DISEASE_DATA.keys()

# curl -X GET http://127.0.0.1:5000/bevan/ -d query="treatments for headaches"

# what are the symptoms of cancer
# treatments for headaches

def get_closest(x):
    vals = [(d, jellyfish.jaro_winkler(x, d.strip().lower())) for d in diseases]
    return max(vals, key=lambda x:x[1])

@app.route("/bevan/", methods=['GET'])
def search():
    qry = request.form.get('query', '').strip()
    if qry:
        nouns = nlp(unicode(qry)).noun_chunks
        scores = dict()
        for n in nouns:
            scores[n] = get_closest(n.text.lower())

        max_score = max(scores.items(), key=lambda x: x[1][1])
        word = max_score[1][0]
        return DISEASE_DATA[word]

    return "No query sent"

if __name__ == "__main__":
    app.run()
