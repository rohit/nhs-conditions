
## To run the Crawler written in Clojure:

- Install [Leiningen](https://leiningen.org/)
- Create a jar: `lein uberjar`
- Run: `java -Xmx4g -jar target/uberjar/crawler-standalone.jar` which produces `choices.json` file.

### TODO:

- Respectfully get URLS with respect to robots.txt
- Error handling and retry behaviour
- Save link graph



## To run API server written in Python:

- Install dependencies: `pip install -r requirements.txt`
- Download English dict: `python -m spacy download en`
- Start server: `python server.py`
- In another terminal window, run: `curl -X GET http://127.0.0.1:5000/bevan/ -d query="what are the symptoms of cancer"`
