(defproject conditions "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [enlive "1.1.6"]
                 [clj-http "3.6.1"]
                 [clojurewerkz/urly "1.0.0"]
                 [org.clojure/tools.logging "0.4.0"]
                 [cheshire "5.7.1"]]
  :main ^:skip-aot conditions.core
  :target-path "target/%s"
  :uberjar-name "crawler-standalone.jar"
  :profiles {:uberjar {:aot :all}})
