(ns conditions.pages
  (:require [clj-http.client :as client]
            [clojure.string :as str]
            [clojurewerkz.urly.core :as url]
            [clojure.tools.logging :as log]
            [net.cgrand.enlive-html :as html])
  (:import [java.net URI]))

(defn get-snippet
  [page]
  (try
    (-> (client/get page)
        :body
        html/html-snippet)
    (catch Exception e)))

(defn- safe-resolve
  [x y]
  (try
    (url/resolve x y)
    (catch Exception e)))

(defn get-conditions
  [page]
  (log/info "Getting conditions from: " page)
  (let [xs   (-> page
                 get-snippet
                 (html/select [:.index-section :ul]))]
    (->> xs
         (map (fn [x]
                (->> (html/select x [:a])
                     (map #(get-in % [:attrs :href]))
                     (map #(safe-resolve page (str/trim %)))
                     (filter identity)
                     (map str/lower-case)
                     (map #(str % "?nobeta=true"))
                     (filter #(str/includes? % "/conditions/")))))
         (apply concat)
         set)))

(defn get-index-pages
  [page]
  (let [indices  (-> page
                     get-snippet
                     (html/select [:#haz-mod1 :a]))]
    (->> indices
         (map #(get-in % [:attrs :href]))
         (map #(safe-resolve page %))
         (filter identity))))

(defn- get-main-content
  [page-frag]
  (let [x  (or (-> (html/select page-frag [:.main-content])
                   first)
               (-> (html/select page-frag [:.guidespanel])
                   first))]
    (->> x
         :content
         html/texts
         (map str/trim)
         (remove str/blank?))))

(defn- get-sub-section
  [page]
  (let [snippet (get-snippet page)]
    [(-> (html/select snippet [:.sub-nav :.active-text]) first :content last)
     (get-main-content snippet)]))

(defn get-condition
  [page]
  (let [p          (client/get page)
        url        (or (last (:trace-redirects p))
                       page)
        main-page  (html/html-snippet (:body p))
        tabs       (->> (html/select main-page [:.sub-nav :a])
                        (map #(get-in % [:attrs :href]))
                        (map #(safe-resolve url %))
                        (filter identity)
                        (map get-sub-section)
                        (into {}))
        title      (->> (html/select main-page [:h1])
                        first
                        :content
                        (str/join " ")
                        str/trim)
        content    (get-main-content main-page)]
    {:title title
     :original-url page
     :url   url
     :tabs tabs
     :content content}))

