(ns conditions.core
  (:require [conditions.pages :as pages]
            [clojure.set :as set]
            [clojure.tools.logging :as log]
            [cheshire.core :as json]
            [clojure.java.io :as io])
  (:gen-class))

(def START-PAGE "http://www.nhs.uk/Conditions/Pages/hub.aspx")

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [indices     (pages/get-index-pages START-PAGE)
        conditions  (reduce (fn [acc x]
                              (set/union acc
                                         (pages/get-conditions x)))
                            #{}
                            indices)]
    (log/infof "Getting %s conditions" (count conditions))
    (json/generate-stream (pmap pages/get-condition conditions)
                          (io/writer "choices.json")
                          {:pretty true})
    (log/info "File written!")
    (shutdown-agents)))
